# ess-linux docker image

[Docker](https://www.docker.com) image based on ESS Linux.

Docker pull command:

```
docker pull registry.esss.lu.se/ics-docker/ess-linux:latest
```
